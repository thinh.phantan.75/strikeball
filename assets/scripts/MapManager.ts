import { BallController } from './BallController';
import { PitController } from './PitController';

import { _decorator, Component, Node, Vec4, Color, randomRangeInt, Material, resources, Prefab, instantiate, director, Scene, find, Camera } from 'cc';
import { MapName, EventName } from './GameDefine';
import gameEvent from './GameEvent';
const { ccclass, property } = _decorator;

@ccclass('MapManager')
export class MapManager extends Component {
    @property(Material) groundMaterial: Material = null
    @property(Material) discMaterial: Material = null
    @property(Material) boderMaterial: Material = null
    @property(Prefab) pitPrefabs: Prefab[] = []
    @property(Material) pitMaterial: Material = null
    @property(Material) pitBreakMaterial: Material = null
    @property(Node) stageClear: Node = null

    private pits: PitController[] = []
    private currentMapIndex: number = 0
    private mapPrefabs: Prefab[] = []
    private mapCurrent: Node = null
    private pitHolder: Node = null

    private ball: Node = null
    private scene: Scene = null

    private discNumber: number = 0
    private pitBreakCounter: number = 0


    private colors_background = [
        [new Vec4(1., 1., 1., 1.), new Vec4(.96, .32, .65, 1.), new Vec4(.4, .4, .4, 1.)],
        [new Vec4(.96, .91, .26, 1.), new Vec4(.96, .32, .32, 1.), new Vec4(.49, .24, 1., 1.)],
        [new Vec4(1., .55, .06, 1.), new Vec4(.46, .32, 1., 1.), new Vec4(1., .2, .42, 1.)],
        [new Vec4(.95, .6, .89, 1.), new Vec4(.87, .22, .24, 1.), new Vec4(0., .73, .86, 1.)],
        [new Vec4(.82, .8, .99, 1.), new Vec4(.95, .25, .61, 1.), new Vec4(.16, .8, .38, 1.)],
        [new Vec4(.61, .92, .6, 1.), new Vec4(.08, .71, .55, 1.), new Vec4(.99, .64, 0., 1.)],
    ]

    private colors_pit = [
        new Vec4(1., 1., .5, 1.), new Vec4(.5, 1., 1., 1.),
        new Vec4(1., .5, 1., 1.), new Vec4(.5, 1., .5, 1.),
        new Vec4(1., .5, .5, 1.), new Vec4(.5, .5, 1., 1.),
    ]


    onLoad() {
        this.setColors()
        this.loadMap(this.currentMapIndex)
        this.scene = director.getScene()
        this.pitHolder = find('pitHolder')
        gameEvent.on(EventName.OnUpdatePitBreakCounter, this.updatePitBreakCounter, this)

    }

    setColors() {
        let colors = this.colors_background[randomRangeInt(0, this.colors_background.length)]

        let groundColor = colors[0].clone().multiplyScalar(255)
        this.setMaterialColor(this.groundMaterial, groundColor)

        let boderColor = colors[1].clone().multiplyScalar(255)
        this.setMaterialColor(this.boderMaterial, boderColor)

        let discColor = new Vec4(.71, .91, .23, 1.).multiplyScalar(255)
        this.setMaterialColor(this.discMaterial, discColor)

        let backgroundCamera = find('Background/Camera').getComponent(Camera)
        let color = colors[2].clone().multiplyScalar(255)
        backgroundCamera.clearColor = new Color(color.x, color.y, color.z, color.w)
    }

    setMaterialColor(material: Material, colorVec: Vec4) {
        let color = new Color(colorVec.x, colorVec.y, colorVec.z, colorVec.w)
        material.setProperty('mainColor', color)
    }

    loadMap(index) {
        this.currentMapIndex = index
        let mapNames = Object.keys(MapName)
        let mapName = mapNames[index]
        resources.load<Prefab>("prefabs/maps/" + mapName, (err, mapPrefab) => {
            if (mapPrefab) {
                this.mapPrefabs[index] = mapPrefab
                this.initMap(index)
            } else {
                console.warn("Load map: " + mapName + " failed!!!");
            }
        });
    }

    initMap(index: number) {
        this.mapCurrent?.destroy()
        let mapPrefab = this.mapPrefabs[index]
        let map = instantiate(mapPrefab)
        map.active = true
        this.mapCurrent = map
        this.scene.addChild(map)
        this.initBall()
        this.initPit()
        this.setCamera()
    }

    initBall() {
        let char = this.mapCurrent.getChildByName("char")
        if (!this.ball) {
            resources.load<Prefab>("prefabs/ball", (err, ballPrefab) => {
                if (ballPrefab) {
                    this.ball = instantiate(ballPrefab)
                    this.ball.active = true
                    this.scene.addChild(this.ball)
                    this.ball.getComponent(BallController).init(char.position)
                } else {
                    console.warn("Load ball failed!!!");
                }
            });
        } else {
            this.ball.getComponent(BallController).init(char.position)
        }
    }

    initPit() {
        this.mapCurrent.children.forEach(child => {
            if (child.name.includes("disc")) {
                this.discNumber++
                let pitPrefab
                if (child.name.includes("hard")) {
                    pitPrefab = this.pitPrefabs[0]
                } else {
                    if (child.name.includes("move")) {
                        pitPrefab = this.pitPrefabs[2]
                    } else {
                        pitPrefab = this.pitPrefabs[1]
                    }
                }

                let pit = instantiate(pitPrefab)
                pit.position = child.position
                pit.active = true
                this.pitHolder.addChild(pit)
                let pitController = pit.getComponent(PitController)
                this.pits.push(pitController)
            }
        })

        this.setPitsColor()
    }

    setPitsColor() {
        this.pits.forEach(pit => {
            let index = randomRangeInt(0, this.colors_pit.length)
            let color = this.colors_pit[index].clone().multiplyScalar(255.);
            pit.setColor(this.pitMaterial, this.pitBreakMaterial, new Color(color.x, color.y, color.z, 255));
        })
    }

    setCamera() {
        let mainCamera = find('Main Camera')
        let mapCamera = this.mapCurrent.getChildByName('Camera')
        mainCamera.setPosition(mapCamera.position)
    }

    resetMap() {
        this.discNumber = 0
        this.pitBreakCounter = 0
        this.stageClear.active = false
        this.mapCurrent.destroy()
        this.pitHolder.removeAllChildren()
    }

    updatePitBreakCounter() {
        this.pits = []
        this.pitBreakCounter++

        if (this.pitBreakCounter == this.discNumber) {
            this.endGame()
        }
    }

    endGame() {
        this.stageClear.active = true
        let ballController = this.ball.getComponent(BallController)
        ballController.offEvent()
        ballController.setEnd()
        setTimeout(() => {
            this.resetMap()
            this.loadMap(++this.currentMapIndex)
        }, 5000)
    }
}
