
import { _decorator, Component, Node, MeshCollider, MeshRenderer, RigidBody, Material, PhysicMaterial } from 'cc';
const { ccclass, property, executeInEditMode } = _decorator;

@ccclass('PitBreakController')
@executeInEditMode(true)
export class PitBreakController extends Component {
    // @property(Material) mat: Material = null
    // @property(Material) discMaterial: Material = null
    @property(Material) groundMaterial: Material = null
    @property(Material) borderMaterial: Material = null
    // @property(PhysicMaterial) border: PhysicMaterial = null
    // @property(PhysicMaterial) ground: PhysicMaterial = null

    onEnable() {
        // this.node.children.forEach((child) => {
        //     if (!child.getComponent(MeshCollider)) {
        //         let collider = child.addComponent(MeshCollider)
        //         let meshRenderer = child.getComponent(MeshRenderer)
        //         collider.mesh = meshRenderer.mesh
        //         collider.convex = true
        //         child.addComponent(RigidBody)
        //         meshRenderer.setMaterial(this.mat, 0)
        //     }
        //     let meshRenderer = child.getComponent(MeshRenderer)
        //     meshRenderer.setMaterial(this.mat, 0)
        // })

        console.log("Start")

        this.node.children.forEach(child => {
            if (child.name.includes('Cube') || child.name.includes('ground') || child.name.includes('Cylinder')) {
                if (!child.getComponent(MeshCollider)) {
                    let collider = child.addComponent(MeshCollider)
                    let meshRenderer = child.getComponent(MeshRenderer)
                    collider.mesh = meshRenderer.mesh
                    let rigidBody = child.addComponent(RigidBody)
                    rigidBody.isStatic = true
                }
            }
            let meshRenderer = child.getComponent(MeshRenderer)
            
            if (child.name.includes('ground')) {
                meshRenderer.setMaterial(this.groundMaterial, 0)
            }

            if (child.name.includes('Cube') || child.name.includes('Cylinder')) {

                meshRenderer.setMaterial(this.borderMaterial, 0)
            }
        })
    }
}
