
import { _decorator, Component, Node, Material, Color, MeshRenderer, resources, Prefab, instantiate, ParticleSystem, RigidBody, Vec3, director, Enum, tween, randomRange, easing } from 'cc';
import { EventName } from './GameDefine';
import gameEvent from './GameEvent';
const { ccclass, property, type } = _decorator;


enum PitType {
    TYPE1,
    TYPE2,
    TYPE3
}

Enum(PitType);

@ccclass('PitController')
export class PitController extends Component {
    @property({ type: PitType })
    type: PitType = PitType.TYPE1
    private pitBreak: Node = null;
    private starPartical: Node = null;
    private isHaftBreak: boolean = false;
    private mat: Material = null
    private breakMat: Material = null

    start() {
        if (this.type == PitType.TYPE3) {
            tween(this.node)
                .delay(randomRange(2, 4))
                .by(.3, { position: new Vec3(0, -2, 0) }, { easing: "sineIn" })
                .delay(randomRange(2, 4))
                .by(.3, { position: new Vec3(0, 2, 0) }, { easing: "sineOut" })
                .union()
                .repeatForever()
                .start()
        }
    }

    setColor(material: Material, pitBreakMaterial: Material,color: Color) {
        this.mat = new Material()
        this.mat.copy(material)
        this.getComponentInChildren(MeshRenderer).setMaterial(this.mat, 0)
        this.mat.setProperty('mainColor', color)

        this.breakMat = new Material()
        this.breakMat.copy(pitBreakMaterial)
        this.breakMat.setProperty('mainColor', color)
        this.initPitBreak()
    }

    private initPitBreak() {
        resources.load<Prefab>("prefabs/pit_break", (err, pitBreakPrefab) => {
            if (pitBreakPrefab) {
                this.pitBreak = instantiate(pitBreakPrefab)
                director.getScene().addChild(this.pitBreak)
                this.pitBreak.setPosition(new Vec3(0, 100, 0))

                this.pitBreak.children.forEach((child) => {
                    child.getComponent(MeshRenderer).setMaterial(this.breakMat, 0)
                    let rigidbody = child.getComponent(RigidBody)
                    rigidbody.isStatic = true
                })
                console.log("Load pit break success!")
            } else {
                console.warn("Load pit break fail, can't spawn !!!")
            }
        })

        resources.load<Prefab>("prefabs/star_particle", (err, starParticalPrefab) => {
            if (starParticalPrefab) {
                this.starPartical = instantiate(starParticalPrefab)
                this.starPartical.active = false
                this.starPartical.setParent(this.node.parent)
                this.starPartical.setPosition(this.node.position)
                console.log("Load star particle success!")
            } else {
                console.warn("Load star particle fail, can't spawn !!!")
            }
        })
    }

    public spawnPitBreak() {
        if (this.starPartical) {
            this.starPartical.active = true
            this.starPartical.getComponent(ParticleSystem)?.play()
        }
        if (this.type == PitType.TYPE1) {
            this.node.children[0].active = false
            if (this.pitBreak) {
                this.pitBreak.children.forEach((child, index) => {
                    if (this.isHaftBreak) {
                        child.getComponent(RigidBody).isStatic = false
                    }
                    if (index >= 6 && !this.isHaftBreak) {
                        child.getComponent(RigidBody).isStatic = false
                    }

                })
                this.pitBreak.setPosition(this.node.position)
            }

            if (this.isHaftBreak) {
                this.updatePitBreakCounter()
                this.fade()
            } else {
                this.isHaftBreak = true
            }
        } else {
            if (this.pitBreak) {
                this.pitBreak.children.forEach((child) => {
                    child.getComponent(RigidBody).isStatic = false
                })
                this.pitBreak.setPosition(this.node.position)
            }

            this.node.active = false
            this.updatePitBreakCounter()
            this.fade()
        }
    }

    private destroyPit() {
        this.node?.destroy()
        this.pitBreak?.destroy()
    }

    private updatePitBreakCounter() {
        gameEvent.emit(EventName.OnUpdatePitBreakCounter);
    }

    private fade() {
        let color: Color
        color = this.breakMat.getProperty('mainColor') as Color

        tween({ alpha: 255 })
            .delay(1)
            .to(2, { alpha: 100 }, {
                onUpdate: (target: any, ratio: number) => {
                    color.a = target.alpha
                    this.breakMat?.setProperty('mainColor', color)
                }, easing: "sineIn"
            })
            .call(() => {
                this.destroyPit()
            })
            .start()
    }
}