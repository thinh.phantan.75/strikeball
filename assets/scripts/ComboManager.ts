
import { _decorator, Component, Node, SpriteFrame, Sprite, tween, Vec3, Tween } from 'cc';
import { EventName } from './GameDefine';
import gameEvent from './GameEvent';
const { ccclass, property } = _decorator;

@ccclass('ComboManager')
export class ComboManager extends Component {
    @property(SpriteFrame) comboSprites: SpriteFrame[] = []
    @property(Sprite) combo: Sprite = null

    private comboCounter: number = 0
    private comboNumer: number = 0
    private tweenScale: Tween<Object> = null

    start() {
        this.comboNumer = this.comboSprites.length
        gameEvent.on(EventName.OnUpdatePitBreakCounter, this.onUpdateCombo, this)
        gameEvent.on(EventName.BallStop, this.onResetCombo, this)
    }

    onUpdateCombo() {
        this.comboCounter++
        if (this.comboCounter > this.comboNumer)
            this.comboCounter = this.comboNumer

        this.combo.spriteFrame = this.comboSprites[this.comboCounter - 1]

        this.tweenScale?.stop()
        this.tweenScale = tween(this.node)
            .to(0.1, { scale: new Vec3(.9, .9, .9) })
            .to(0.1, { scale: Vec3.ONE })
            .start()
    }

    onResetCombo() {
        this.comboCounter = 0
        this.combo.spriteFrame = null
    }
}
