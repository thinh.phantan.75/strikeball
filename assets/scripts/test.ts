import { _decorator, Component, Node, MeshRenderer, utils, gfx, Color, Vec2, Vec3, math, find, instantiate } from 'cc';
const { ccclass, property, executeInEditMode } = _decorator;

@ccclass('test')
@executeInEditMode
export class test extends Component {
    onEnable() {
        // let d = 1.41421356 /2   
        let d = 2
        let vertexs = this.getTestVertexs()
        // let meshVertexs = this.findMeshVertexs(vertexs, d)
        let meshVertex = this.findMeshVertexs(vertexs, d)
        let renderer = this.node.getComponent(MeshRenderer)
        renderer.mesh = utils.createMesh({
            primitiveMode: gfx.PrimitiveMode.LINE_STRIP,
            positions: meshVertex.meshVertexs,
            // normals: [0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0],
            uvs: [0, 0,
                1, 0,
                0, 1,
                1, 1],
            // colors: [
            //     Color.RED.r, Color.RED.g, Color.RED.b, Color.RED.a,
            //     Color.RED.r, Color.RED.g, Color.RED.b, Color.RED.a,
            //     Color.RED.r, Color.RED.g, Color.RED.b, Color.RED.a,
            //     Color.RED.r, Color.RED.g, Color.RED.b, Color.RED.a],
            // attributes: [
            //     new gfx.Attribute(gfx.AttributeName.ATTR_POSITION, gfx.Format.RGB32F),
            //     new gfx.Attribute(gfx.AttributeName.ATTR_TEX_COORD, gfx.Format.RG32F),
            //     new gfx.Attribute(gfx.AttributeName.ATTR_COLOR, gfx.Format.RGBA8UI, true),
            //     new gfx.Attribute(gfx.AttributeName.ATTR_JOINTS, gfx.Format.RGBA8UI, true),
            // ],
            indices: this.findMeshIndices(meshVertex.meshVertexs),
        }, undefined, { calculateBounds: false })

        this.createAssembledKinkMesh(meshVertex.assembledKinkPolygons)
    }

    createAssembledKinkMesh(points: Vec2[]) {
        let assembledKinkMesh = find('assembledKinkMesh')
        assembledKinkMesh.removeAllChildren()
        let tempNode = find('temp')
        for (let i = 0; i < points.length; i += 4) {
            let meshPoints = points.slice(i, i + 4)

            let child = instantiate(tempNode)
            child.setParent(assembledKinkMesh)
            let renderer = child.addComponent(MeshRenderer)
            let mesh = this.getAssembledKinkMesh(meshPoints)
            renderer.mesh = utils.createMesh({
                primitiveMode: gfx.PrimitiveMode.LINE_STRIP,
                positions: mesh.meshVertexs,
                uvs: [0, 0,
                    1, 0,
                    0, 1,
                    1, 1],
                indices: mesh.indices,
            }, undefined, { calculateBounds: false })
        }
    }

    getTestVertexs() {
        let vertexs: Vec2[] = []

        this.node.children.forEach((child) => {
            if (child.active) {
                let pos = child.position
                let vertex = new Vec2(pos.x, pos.z)
                vertexs.push(vertex)
            }
        })

        return vertexs
    }

    findFourPointPerpendicular(x1, y1, x2, y2, H) { //Find A, B, C, D 
        let xA, yA, xC, yC, xB, yB, xD, yD
        if (x1 != x2 && y1 != y2) {
            let dx = (x2 - x1)
            let dy = (y2 - y1)
            let k = H / 2 * Math.sqrt(dx * dx + dy * dy)
            let k2 = dy * dy / dx
            yA = (k + (dx + k2) * y1) / (dx + k2)
            xA = (dx * x1 + dy * y1 - dy * yA) / dx

            yC = (-k + (dx + k2) * y1) / (dx + k2)
            xC = (dx * x1 + dy * y1 - dy * yC) / dx
        } else {
            if (x1 == x2) {
                yA = y1
                xA = x1 - H / 2
                yC = y1
                xC = x1 + H / 2
            } else {
                xA = x1
                xC = x1
                yA = y1 + H / 2
                yC = y1 - H / 2
            }
        }

        xB = x2 + (x1 - xC)
        yB = y2 + (y1 - yC)
        yD = y2 - (y1 - yC)
        xD = x2 - (x1 - xC)

        let A = new Vec2(xA, yA)
        let B = new Vec2(xB, yB)
        let C = new Vec2(xC, yC)
        let D = new Vec2(xD, yD)

        let result = [A, B, D, C]

        return this.isCockwise(result) ? result : result.reverse()
    }

    findMeshVertexs(vertexs, d) {
        let meshVertex: Vec2[] = []
        let n = vertexs.length
        for (let i = 0; i + 1 < n; i++) {
            let vertex = this.findFourPointPerpendicular(vertexs[i].x, vertexs[i].y, vertexs[i + 1].x, vertexs[i + 1].y, d)
            meshVertex = meshVertex.concat(vertex)
        }
        // console.log(meshVertex)
        return this.convertMeshVertex(meshVertex)
    }

    convertMeshVertex(meshVertexs: Vec2[]) {
        let meshVertexResult: number[] = []
        let assembledKinkPolygons: Vec2[] = []
        let n = meshVertexs.length

        // if (n == 4) return meshVertexs

        for (let i = 0; i + 7 < n; i += 4) {
            let eightPoints = meshVertexs.slice(i, i + 8)
            //six points: A, B, D, C, A', B', D', C'
            //AB, A'B'
            let intersection1 = this.lineLineIntersection(eightPoints[0], eightPoints[1], eightPoints[4], eightPoints[5])
            //DC, D'C'
            let intersection2 = this.lineLineIntersection(eightPoints[2], eightPoints[3], eightPoints[6], eightPoints[7])

            let head = [
                eightPoints[0].x, 0, eightPoints[0].y,
                eightPoints[3].x, 0, eightPoints[3].y
            ]

            let mid: number[]
            let assembledKinkPolygon: Vec2[]

            if (this.checkPointInRectangle(eightPoints[0], eightPoints[1], eightPoints[2], eightPoints[3], intersection1)) {
                mid = [
                    intersection1.x, 0, intersection1.y,
                    eightPoints[2].x, 0, eightPoints[2].y,
                    intersection1.x, 0, intersection1.y,
                    eightPoints[7].x, 0, eightPoints[7].y,
                ]

                assembledKinkPolygon = [
                    intersection1, eightPoints[2], intersection2, eightPoints[7]
                ]
            } else {
                mid = [
                    eightPoints[1].x, 0, eightPoints[1].y,
                    intersection2.x, 0, intersection2.y,
                    eightPoints[4].x, 0, eightPoints[4].y,
                    intersection2.x, 0, intersection2.y,
                ],

                    assembledKinkPolygon = [
                        intersection2, eightPoints[1], intersection1, eightPoints[4]
                    ]
            }

            let end = [
                eightPoints[5].x, 0, eightPoints[5].y,
                eightPoints[6].x, 0, eightPoints[6].y
            ]

            if (n == 8) {
                meshVertexResult = meshVertexResult.concat(head, mid, end)
            } else if (i == 0) {
                meshVertexResult = meshVertexResult.concat(head, mid)
            } else if (i + 8 == n) {
                meshVertexResult = meshVertexResult.concat(mid, end)
            } else {
                meshVertexResult = meshVertexResult.concat(mid)
            }

            assembledKinkPolygons = assembledKinkPolygons.concat(assembledKinkPolygon)
        }
        return {
            meshVertexs: meshVertexResult,
            assembledKinkPolygons
        }
    }

    checkPointInRectangle(A: Vec2, B: Vec2, C: Vec2, D: Vec2, M: Vec2) // Check M is in ABCD
    {
        let AB = this.createVec2FromTwoPoint(A, B)
        let AM = this.createVec2FromTwoPoint(A, M)
        let BC = this.createVec2FromTwoPoint(B, C)
        let BM = this.createVec2FromTwoPoint(B, M)
        let dotABAM = Vec2.dot(AB, AM);
        let dotABAB = Vec2.dot(AB, AB);
        let dotBCBM = Vec2.dot(BC, BM);
        let dotBCBC = Vec2.dot(BC, BC);
        return 0 <= dotABAM && dotABAM <= dotABAB && 0 <= dotBCBM && dotBCBM <= dotBCBC;
    }

    createVec2FromTwoPoint(A: Vec2, B: Vec2) {
        return new Vec2(B.x - A.x, B.y - A.y);
    }

    findMeshIndices(meshVertexs: number[]) {
        let meshIndices: number[] = []
        let n = meshVertexs.length / 3
        let points: Vec2[] = []

        for (let i = 0; i < n; i++) {
            let point = new Vec2(meshVertexs[i * 3], meshVertexs[i * 3 + 2])
            points.push(point)
        }

        // console.log(points)

        let index = 0
        for (let i = 0; i + 3 < n; i += 4) {
            let fourPoints = points.slice(i, i + 4)
            let indices = this.findMeshIndicesFourPoints(fourPoints)
            indices.forEach(e => {
                meshIndices.push(e + 4 * index)
            })
            index++
        }

        // console.log(meshIndices)
        return meshIndices
    }

    orientation(a: Vec2, b: Vec2, c: Vec2) {
        return Vec2.cross(a, b) + Vec2.cross(b, c) + Vec2.cross(c, a);
    }

    swap(array, index1, index2) {
        [array[index1], array[index2]] = [array[index2], array[index1]];
    }

    isCockwise(points: Vec2[]) {
        let a = points[0];
        let b = points[1];
        let c = points[2];
        let d = points[3];

        if (this.orientation(a, b, c) < 0.0) {
            if (this.orientation(a, c, d) < 0.0) {
                return true
            }
        }
        return false
    }

    sort4PointsClockwise(points: Vec2[]): Vec2[] {
        // console.log(points)
        let a = points[0];
        let b = points[1];
        let c = points[2];
        let d = points[3];

        if (this.orientation(a, b, c) < 0.0) {
            // Triangle abc is already clockwise.  Where does d fit?
            if (this.orientation(a, c, d) < 0.0) {
                return points;           // Cool!
            } else if (this.orientation(a, b, d) < 0.0) {
                // swap(d, c);
                this.swap(points, 2, 3)
            } else {
                // swap(a, d);
                this.swap(points, 0, 3)
            }
        } else if (this.orientation(a, c, d) < 0.0) {
            // Triangle abc is counterclockwise, i.e. acb is clockwise.
            // Also, acd is clockwise.
            if (this.orientation(a, b, d) < 0.0) {
                // swap(b, c);
                this.swap(points, 1, 2)
            } else {
                // swap(a, b);
                this.swap(points, 0, 1)
            }
        } else {
            // Triangle abc is counterclockwise, and acd is counterclockwise.
            // Therefore, abcd is counterclockwise.
            // swap(a, c);
            this.swap(points, 0, 2)
        }

        return points
    }

    findMeshIndicesFourPoints(points: Vec2[]): number[] {
        let pointsCopy = points.map(x => x)
        let orderedPoints = this.sort4PointsClockwise(pointsCopy)
        let indicesBase = [0, 1, 3, 1, 2, 3]
        let result = []
        indicesBase.forEach((element, index) => {
            result.push(points.indexOf(orderedPoints[element]))
        })
        return result
    }

    lineLineIntersection(A: Vec2, B: Vec2, C: Vec2, D: Vec2): Vec2 {
        // Line AB represented as a1x + b1y = c1
        let a1 = B.y - A.y;
        let b1 = A.x - B.x;
        let c1 = a1 * (A.x) + b1 * (A.y);

        // Line CD represented as a2x + b2y = c2
        let a2 = D.y - C.y;
        let b2 = C.x - D.x;
        let c2 = a2 * (C.x) + b2 * (C.y);

        let determinant = a1 * b2 - a2 * b1;

        if (determinant == 0) {
            // The lines are parallel. This is simplified
            // by returning a pair of FLT_MAX
            return new Vec2(Number.MAX_SAFE_INTEGER, Number.MAX_SAFE_INTEGER);
        }
        else {
            let x = (b2 * c1 - b1 * c2) / determinant;
            let y = (a1 * c2 - a2 * c1) / determinant;
            return new Vec2(x, y)
        }
    }

    getAssembledKinkMesh(points: Vec2[]) {
        let meshVertexs: number[] = []
        let berizePoints: Vec2[] = []
        berizePoints.push(points[0])

        if (points[1].x > points[3].x) {
            this.swap(points, 1, 3)
        } else {
            if (points[1].y > points[3].y) {
                this.swap(points, 1, 3)
            }
        }

        if (this.orientation(points[0], points[1], points[3]) < 0.0) //check is clockwise
        {
            berizePoints = berizePoints.concat(this.getBezierCurve(points[1], points[3], points[2]))
        }else{
            berizePoints = berizePoints.concat(this.getBezierCurve(points[1], points[3], points[2]).reverse())
        }
            
        berizePoints.forEach(point => {
            meshVertexs.push(point.x, 0, point.y)
        })

        let indices = []

        for (let i = 0; i < berizePoints.length - 2; i++) {
            indices.push(0, 1 + i, 2 + i)
        }

        return { meshVertexs, indices }
    }

    getBezierCurve(j1, j2, pa1, segments = 8) {
        let coords = [j1]

        let step = 1.0 / segments
        for (let t = step; 1.0 - t; t += step) {
            let x = ((1 - t) * (1 - t) * j1.x + 2 * (1 - t) * t * pa1.x + t * t * j2.x)
            let y = ((1 - t) * (1 - t) * j1.y + 2 * (1 - t) * t * pa1.y + t * t * j2.y)
            coords.push(new Vec2(x, y))
        }
        coords.push(j2)
        return coords
    }

    // ======================== DON'T USE ==========================

    findMeshVertexs2(vertexs, d) {
        let meshVertex = []
        let n = vertexs.length
        for (let i = 0; i + 2 < n; i++) {
            let vertex = this.findTwoPointBisectorWithDistance(vertexs[i].x, vertexs[i].y, vertexs[i + 1].x, vertexs[i + 1].y, vertexs[i + 2].x, vertexs[i + 2].y, d)
            meshVertex = meshVertex.concat(vertex)
        }
        return meshVertex
    }

    findTwoPointBisectorWithDistance(xA, yA, xB, yB, xC, yC, D) {
        let BA = new Vec2(xA - xB, yA - yB)
        let BC = new Vec2(xC - xB, yC - yB)

        BA.normalize()
        BC.normalize()
        let BE = BA.add(BC)

        if (BE.equals(Vec2.ZERO)) {
            if (xA != xB) {
                BE = new Vec2(0, 1)
            } else {
                BE = new Vec2(1, 0)
            }
        } else {
            let k = D / BE.length()
            BE = BE.multiplyScalar(k)
        }

        let xE = xB + BE.x
        let yE = yB + BE.y
        let xF = xB * 2 - xE
        let yF = yB * 2 - yE

        let vertex: number[] = [
            xE, 0, yE,
            xF, 0, yF,
        ]

        return vertex
    }

    sort4PointsClockwiseFromFirst(points: Vec2[]): Vec2[] {
        // console.log(points)
        let a = points[0];
        let b = points[1];
        let c = points[2];
        let d = points[3];

        if (this.orientation(a, b, c) < 0.0) {
            // Triangle abc is already clockwise.  Where does d fit?
            if (this.orientation(a, c, d) < 0.0) {
                return points;           // Cool!
            } else if (this.orientation(a, b, d) < 0.0) {
                // swap(d, c);
                this.swap(points, 2, 3)
            } else {
                // swap(a, d);
                // this.swap(points, 0, 3)
                this.swap(points, 1, 2)
                this.swap(points, 2, 3)
            }
        } else if (this.orientation(a, c, d) < 0.0) {
            // Triangle abc is counterclockwise, i.e. acb is clockwise.
            // Also, acd is clockwise.
            if (this.orientation(a, b, d) < 0.0) {
                // swap(b, c);
                this.swap(points, 1, 2)
            } else {
                // swap(a, b);
                // this.swap(points, 0, 1)
                this.swap(points, 1, 3)
                this.swap(points, 2, 3)
            }
        } else {
            // Triangle abc is counterclockwise, and acd is counterclockwise.
            // Therefore, abcd is counterclockwise.
            // swap(a, c);
            this.swap(points, 1, 3)
        }

        return points
    }
}