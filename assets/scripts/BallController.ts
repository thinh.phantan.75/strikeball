import { AudioManager } from './AudioManager';
import { PitController } from './PitController';

import { _decorator, Component, Node, find, EventTouch, RigidBody, Vec3, Collider, ITriggerEvent, ICollisionEvent } from 'cc';
import { Layer, SoundName, EventName } from './GameDefine';
import gameEvent from './GameEvent';
const { ccclass, property } = _decorator;

@ccclass('BallController')
export class BallController extends Component {
    private rb: RigidBody = null
    private collider: Collider = null;
    private scaler: number = 3
    private maxLength: number = 300
    private arrow: Node = null
    private canvas: Node = null
    private isEnd: boolean = false

    onLoad() {
        this.canvas = find('Canvas')
        this.rb = this.node.getComponent(RigidBody)
        this.rb.linearFactor = new Vec3(1, 0, 1)

        this.collider = this.node.getComponent(Collider)
        this.collider.on("onTriggerEnter", this.onTriggerEnter, this)
        this.collider.on("onCollisionEnter", this.onCollisionEnter, this)

        this.arrow = find('arrow')
        this.arrow.active = false
    }

    update(dt: number) {
        this.arrow.position = this.node.position
        let outVec: Vec3 = new Vec3
        this.rb.getLinearVelocity(outVec)
        if (outVec.length() < 0.6) {
            gameEvent.emit(EventName.BallStop)
            this.rb.clearVelocity()
            this.onEvent()
        }
    }

    public init(pos) {
        this.isEnd = false
        this.node.setPosition(pos)
        this.onEvent()
    }

    public setEnd() {
        this.isEnd = true
    }

    public onEvent() {
        if (this.isEnd) return
        this.canvas.on(Node.EventType.TOUCH_START, this.ontouchstart, this)
        this.canvas.on(Node.EventType.TOUCH_END, this.ontouchend, this)
        this.canvas.on(Node.EventType.TOUCH_MOVE, this.ontouchmove, this)
        this.canvas.on(Node.EventType.TOUCH_CANCEL, this.ontouchend, this)
    }

    public offEvent() {
        this.canvas.off(Node.EventType.TOUCH_START, this.ontouchstart, this)
        this.canvas.off(Node.EventType.TOUCH_END, this.ontouchend, this)
        this.canvas.off(Node.EventType.TOUCH_MOVE, this.ontouchmove, this)
        this.canvas.off(Node.EventType.TOUCH_CANCEL, this.ontouchend, this)
    }

    private ontouchstart(event: EventTouch) {
        this.arrow.active = true
        this.arrow.position = this.node.position
        this.arrow.setScale(1, 1, 0)
    }

    private ontouchend(event: EventTouch) {
        this.offEvent()
        this.arrow.active = false
        let direction = event.getStartLocation().subtract(event.getLocation())

        let force = new Vec3(direction.y, 0, direction.x)
        let length = direction.length()
        force = force.normalize().multiplyScalar(Math.min(length, this.maxLength) * this.scaler)
        this.rb.applyImpulse(force)
    }

    private ontouchmove(event: EventTouch) {
        let direction = event.getStartLocation().subtract(event.getLocation())
        this.arrow.setScale(1, 1, Math.min(direction.length(), this.maxLength) / this.maxLength)
        this.arrow.forward = new Vec3(direction.y, 0, direction.x)
    }

    private onTriggerEnter(event: ITriggerEvent) {
        let pitController = event.otherCollider.node.getComponent(PitController)
        pitController?.spawnPitBreak()
        AudioManager.Instance.playSfx(SoundName.Break1)
    }

    private onCollisionEnter(event: ICollisionEvent) {
        if (event.otherCollider.node.layer == Layer.Wall) {
            AudioManager.Instance.playSfx(SoundName.Wall)
        }
    }
}
