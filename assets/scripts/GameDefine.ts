export class Layer {
    static readonly Ball: number = 1 << 0;
    static readonly Wall: number = 1 << 1;
}

export enum MapName {
    map_001 = "map_001",
    map_002 = "map_002",
    map_003 = "map_003",
    map_004 = "map_004",
    map_005 = "map_005",
    map_006 = "map_006",
    map_007 = "map_007",
    map_008 = "map_008",
    map_009 = "map_009",
    map_010 = "map_010",
    map_011 = "map_011",
    map_012 = "map_012",
    map_013 = "map_013",
    map_014 = "map_014",
    map_015 = "map_015",
    map_016 = "map_016",
    map_017 = "map_017",
    map_018 = "map_018",
    map_019 = "map_019",
    map_020 = "map_020"
}


export enum SoundName {
    Break1 = "break1",
    Break2 = "break2",
    Break3 = "break3",
    Clear = "clear",
    Tap01 = "TAP-01",
    Tap02 = "TAP-02",
    Wall = "wall"
}

export enum EventName {
    OnUpdatePitBreakCounter = "on-update-pit-break-counter",
    BallStart = "on-ball-start",
    BallStop = "on-ball-stop"
}