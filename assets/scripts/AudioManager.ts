
import { _decorator, Component, Node, resources, AudioClip, AudioSource } from 'cc';
import { SoundName } from './GameDefine';
const { ccclass, property } = _decorator;
 
@ccclass('AudioManager')
export class AudioManager extends Component {
    private audioClips: AudioClip[] = []
    private bgVolume: number = 0.4
    private sfxVolume: number = 0.7

    audioSourceSFX: AudioSource = null
    audioSourceMusic: AudioSource = null

    public IsEnable: boolean = true

    static instance: AudioManager = null
    public static get Instance() {
        return this.instance
    }

    onLoad() {
        AudioManager.instance = this
        this.audioSourceSFX = this.addComponent(AudioSource)
        this.audioSourceSFX.loop = false
        this.audioSourceSFX.volume = this.sfxVolume
        this.audioSourceSFX.playOnAwake = false
        this.audioSourceMusic = this.addComponent(AudioSource)
        this.audioSourceMusic.playOnAwake = false
        this.audioSourceMusic.play()
        this.loadSound()
    }

    public playMusic(clip: AudioClip, loop: boolean) {
        if (clip && this.IsEnable) {
            if (this.audioSourceMusic.clip != clip || !this.audioSourceMusic.playing) {
                this.stop()
                this.audioSourceMusic.clip = clip
                this.audioSourceMusic.loop = loop
                this.on()
                this.audioSourceMusic.volume = this.bgVolume
                this.audioSourceMusic.play()
            }
        }
    }

    public stop() {
        this.audioSourceMusic.stop()
        this.audioSourceSFX.stop()
    }

    public Ssart() {
        if (this.IsEnable)
            this.audioSourceMusic.play()
    }

    public playSfx(name: SoundName) {
        if(!this.IsEnable) return
        let clip = this.audioClips.find(clip => clip.name == name)
        if (clip) {
            this.audioSourceSFX.playOneShot(clip)
        }
    }

    public StopSfx(name: SoundName) {
        this.audioClips.forEach(clip => {
            if (clip.name == name && this.audioSourceSFX.clip == clip) {
                if (this.audioSourceSFX.playing) {
                    this.audioSourceSFX.stop()
                }
            }
            return
        });
    }

    public playMusicWithName(name: SoundName, loop: boolean) {
        if(!this.IsEnable) return
        this.audioClips.forEach(clip => {
            if (clip.name == name) {
                this.playMusic(clip, loop)
            }
            return;
        });
    }

    public on() {
        this.IsEnable = true
        this.start()
    }

    public off() {
        this.IsEnable = false
        this.stop()
    }

    public nextStatus() {
        this.IsEnable = !this.IsEnable
        if (this.IsEnable) {
            this.on()
        } else {
            this.off()
        }
    }

    public loadSound() {
        let array = Object.keys(SoundName)
        array.forEach(element => {
            let name = SoundName[element]
            resources.load<AudioClip>("sounds/" + name, (err, audioClip) => {
                if (audioClip) {
                    audioClip.name = name
                    this.audioClips.push(audioClip)
                    console.log("Sound loaded: " + name)
                }
                else {
                    console.error("Sound notfound: " + name)
                }
            })
        });
    }
}
